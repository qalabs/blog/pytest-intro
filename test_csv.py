﻿def test_total_shares(total_shares):
    """Checks if shares sum up to 100"""
    assert total_shares == 100
